//Modules indispensables
const express = require('express');
const log = require('metalogger')();

const app = express.Router();

//Modules spécifiques
const mysql = require('mysql2');
const DAO = require('./DAO');


// Create the connection pool. The pool-specific settings are the defaults
if (!process.env.MYSQL_HOST) {
  throw "Aucun hote mysql renseigné dans .env ; éventuellement, c'est qu'il n'y a pas de fichier d'environnement .env détecté : copiez .env.example en .env, puis ajustez les paramètres."
}
const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  port: process.env.MYSQL_PORT,
  database: process.env.MYSQL_DATABASE,
  waitForConnections: process.env.MYSQL_WAITFORCONNECTIONS,
  connectionLimit: process.env.MYSQL_CONNECTIONLIMIT,
  queueLimit: process.env.MYSQL_QUEUELIMIT,
});
const promisePool = pool.promise();

const ID_SALLES_COLLABORATION_NANTES = 1;
const ID_SALLES_COLLABORATION_BREST = 5;

//Exemple : const [rows, fields] = await promisePool.execute('SELECT * FROM `grr_area` WHERE `name` = ? AND `age` > ?', ['Morty', 14]);

require('./common')(app, DAO, promisePool)
require('./materiel')(app, DAO, promisePool)
require('./salles')(app, DAO, promisePool)


module.exports = app;
