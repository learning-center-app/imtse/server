const path = require('path');

module.exports = {
  entry: {
    'webpack.css.js' : path.resolve(__dirname, 'api','prismic','css','webpack.css.js')
  },
  output: {
    path: path.resolve(__dirname, 'api','prismic','css'),
    filename: 'page.css'
  },

  module: {
    rules: [{
      test: /\.scss$/,
      loaders: [
        {
          loader: 'style-loader', // inject CSS to page
          options: {
            singleton: true
          }
        },
        {
          loader: 'css-loader', // translates CSS into CommonJS modules
        }, {
          loader: 'postcss-loader', // Run post css actions
          options: {
            plugins: function () { // post css plugins, can be exported to postcss.config.js
              return [
                require('precss'),
                require('autoprefixer')
              ];
            }
          }
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }
      ]
    },
    {
      test: /\.(png|jpg)$/,
      loader: 'url-loader'
    },
    {
      test: /\.(otf)$/,
      use: {
        loader: "file-loader",
        options: {
          name: "./api/prismic/fonts/[name].[ext]",
        },
      },
    },]
  }
}
